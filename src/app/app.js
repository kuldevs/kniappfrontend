/*
 * Copyright (c) 2016 Paweł Partyka <partyka95@icloud.com>
 */

import angular from 'angular';
import uiRouter from 'angular-ui-router';

import Common from '../common/common';
import Components from '../components/components';
import Entities from '../entities/entities';

import template from './app.html';
import controller from './app.controller';

import './app.scss';

export default angular
    .module('kniApp', [
        uiRouter,

        Common.name,
        Components.name,
        Entities.name
    ])
    .config(['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) => {
        $urlRouterProvider.otherwise('');

        $stateProvider
            .state('app', {
                url: '/',
                views: {
                    'content': { template: 'testyyyy' },
                    'navbar': { template: 'navbar' },
                    'footer': { template: 'footer' }
                }
            })
        ;
    }])
    .directive('app', {
        restrict: 'E',
        template: template,
        controller: controller
    })
;