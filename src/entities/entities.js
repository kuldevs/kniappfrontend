/*
 * Copyright (c) 2016 Paweł Partyka <partyka95@icloud.com>
 */

import angular from 'angular';

export default angular
    .module('entities', [])
;